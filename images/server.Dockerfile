FROM alpine

WORKDIR /bin/tictactoe

COPY /out/server .

CMD ["./server"]
