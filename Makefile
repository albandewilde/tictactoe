.PHONY: clean test grpc-go server server-run server-image client-go-cli client-go-cli-run

##################################################
clean:
	@rm -rf ./out/*

test:
	@go test ./... -v

################################################## Code generation

# Generate grpc and serealisation code for go
grpc-go:
	protoc --go_out=./grpc --go_opt=paths=import \
		--go-grpc_out=./grpc --go-grpc_opt=paths=import \
		./proto/tictactoe.proto

################################################## Server

server:
	@CGO_ENABLED=0 go build -o ./out/server ./server/

server-run: server
	@./out/server

server-image:
	@docker build --no-cache -t tictactoe-server -f ./images/server.Dockerfile .

################################################## Clients

client-go-cli:
	@CGO_ENABLED=0 go build -o ./out/client_go_cli ./clients/go_cli_client/

client-go-cli-run: client-go-cli
	@./out/client_go_cli
