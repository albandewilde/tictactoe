package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"strconv"

	"github.com/google/uuid"
	"google.golang.org/grpc"

	"gitlab.com/albandewilde/tictactoe/grpc/tictactoepb"
)

const HOST = "127.0.0.1"
const PORT = 4444

func main() {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	conn, err := grpc.Dial(fmt.Sprintf("%s:%d", HOST, PORT), opts...)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	client := tictactoepb.NewTictactoeClient(conn)

	start(client)
}

func start(c tictactoepb.TictactoeClient) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	player := &tictactoepb.Player{
		Name: "Moi",
		Id:   uuid.NewString(),
	}

	// Send start game
	fmt.Println("Connecting to the server...")
	stream, err := c.Start(ctx, player)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Waiting for an other player to join")
	if err := play(player, stream, c); err != nil {
		log.Fatal(err)
	}
}

func play(player *tictactoepb.Player, stream tictactoepb.Tictactoe_StartClient, client tictactoepb.TictactoeClient) error {
	board := []rune{'*', '*', '*', '*', '*', '*', '*', '*', '*'}

	for {
		turn, err := stream.Recv()
		if err == io.EOF {
			return nil
		} else if err != nil {
			return err
		}

		// Complete the board
		place := turn.Move.Place - 1
		if place == -1 {
			// It's the tunr zero (used to initialization)
		} else {
			if turn.Move.Player.Id == player.Id {
				board[place] = 'O'
			} else {
				board[place] = 'X'
			}
		}

		// Clear console before displaying the board
		fmt.Print("\033[2J")
		displayBoard(board)
		fmt.Println()

		// Check if the game is over
		if turn.GameOver {
			displayWinner(turn.WinnerId, player.Id)
			return nil
		}

		// Check if it's out turn
		if turn.Move.Player.Id == player.Id {
			// Wait for other player to play
			fmt.Println("Waiting for other player to do his move.")
		} else {
			// It's out turn, do our move
			move(client, player, turn.Move.GameId)
		}
	}
}

func displayWinner(winnerID, playerID string) {
	if winnerID == playerID {
		fmt.Println("YOU WON !")
	} else if winnerID == "" {
		fmt.Println("Draw")
	} else {
		fmt.Println("You lose.")
	}
}

func displayBoard(b []rune) {
	fmt.Printf(
		"%c %c %c\n%c %c %c\n%c %c %c\n",
		b[0], b[1], b[2],
		b[3], b[4], b[5],
		b[6], b[7], b[8],
	)
}

func move(client tictactoepb.TictactoeClient, player *tictactoepb.Player, gameID string) {
	fmt.Print("Your turn: ")

	move := scanMove()
	err := sendMove(client, move, player, gameID)
	for err != nil {
		fmt.Print("Enter a number between 1 & 9: ")
		move = scanMove()
		err = sendMove(client, move, player, gameID)
	}
}

func scanMove() int {
	var place string
	fmt.Scanln(&place)

	// Convert the given place to an int
	var move int
	var err error
	move, err = strconv.Atoi(place)
	for err != nil {
		fmt.Print("Enter a number between 1 & 9: ")
		fmt.Scanln(&place)
		move, err = strconv.Atoi(place)
	}

	return move
}

func sendMove(client tictactoepb.TictactoeClient, move int, player *tictactoepb.Player, gameID string) error {
	_, err := client.Play(
		context.Background(),
		&tictactoepb.Move{
			Place:  int32(move),
			Player: player,
			GameId: gameID,
		},
	)
	return err
}
