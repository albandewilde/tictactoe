package main

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/albandewilde/tictactoe/grpc/tictactoepb"
	"gitlab.com/albandewilde/tictactoe/server/tictactoe"
)

// TictactoeServer implements the Tictactoe service
type TictactoeServer struct {
	tictactoepb.UnimplementedTictactoeServer

	// Turns to send
	ts chan tictactoepb.Turn
	// Clients connected
	p chan *playerStreamGame
	// Games in progress
	games map[string]*game
}

// PlayerStream regroup the player, his stream and the game he is in
type playerStreamGame struct {
	player *tictactoepb.Player
	stream tictactoepb.Tictactoe_StartServer
	game   *game
	moves  chan *tictactoepb.Move
	tttp   tictactoe.Player
}

// Game regroup two players and the board they play on
type game struct {
	p0    *playerStreamGame
	p1    *playerStreamGame
	board tictactoe.Board
}

func newTictactoeServer() *TictactoeServer {
	s := &TictactoeServer{
		ts:    make(chan tictactoepb.Turn),
		p:     make(chan *playerStreamGame),
		games: make(map[string]*game),
	}

	go s.dispatchPlayers()
	return s
}

// Start implement the tictactoe interface
func (s *TictactoeServer) Start(player *tictactoepb.Player, stream tictactoepb.Tictactoe_StartServer) error {
	psg := &playerStreamGame{
		player: player,
		stream: stream,
		moves:  make(chan *tictactoepb.Move),
	}

	s.p <- psg

	for {
		if psg.game == nil {
			continue
		}

		// Get the move done
		move := <-psg.moves

		// Check if game is over
		gameOver, winner := psg.game.board.IsGameOver()
		// Find the winner
		var winnerID string
		if winner == psg.game.p0.tttp {
			winnerID = psg.game.p0.player.Id
		} else if winner == psg.game.p1.tttp {
			winnerID = psg.game.p1.player.Id
		}

		// Send move to players connected to our game
		turn := &tictactoepb.Turn{
			Move:     move,
			GameOver: gameOver,
			WinnerId: winnerID,
		}

		psg.game.p0.stream.Send(turn)
		psg.game.p1.stream.Send(turn)
	}
}

// DispatchPlayers regroup players by two and start the ttt game
func (s *TictactoeServer) dispatchPlayers() {
	for {
		p0 := <-s.p
		p1 := <-s.p

		go s.startGame(p0, p1)
	}
}

// StartGame with two players
func (s *TictactoeServer) startGame(p0, p1 *playerStreamGame) {
	// Generate the game id
	id := uuid.NewString()

	game := &game{
		p0:    p0,
		p1:    p1,
		board: tictactoe.NewBoard(),
	}

	p0.game = game
	p0.tttp = tictactoe.PlayerOne
	p1.game = game
	p1.tttp = tictactoe.PlayerTwo

	s.games[id] = game

	// Prevent user that p1 begin the game
	turnZero := &tictactoepb.Turn{
		Move: &tictactoepb.Move{
			Place:  0,
			Player: p0.player, // This turn is made by p0, so it's p1 turn
			GameId: id,
		},
		GameOver: false,
		WinnerId: "",
	}

	err := p0.stream.Send(turnZero)
	if err != nil {
		fmt.Println(err)
	}
	err = p1.stream.Send(turnZero)
	if err != nil {
		fmt.Println(err)
	}
}

// Play implement the tictactoe interface
func (s *TictactoeServer) Play(ctx context.Context, move *tictactoepb.Move) (*tictactoepb.Move, error) {
	playerID := move.Player.Id
	// Find the player
	var psg *playerStreamGame
	for _, game := range s.games {
		if game.p0.player.Id == playerID {
			psg = game.p0
			break
		} else if game.p1.player.Id == playerID {
			psg = game.p1
			break
		}
	}

	// Put the move on the board
	err := psg.game.board.Move(tictactoe.Cell(move.Place-1), psg.tttp)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	psg.moves <- move

	return move, nil
}
