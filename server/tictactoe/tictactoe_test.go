package tictactoe

import (
	"errors"
	"reflect"
	"testing"
)

func TestCreateANewTictactoe(t *testing.T) {
	cases := []struct {
		board    Board
		expected bool
	}{
		{
			board:    NewBoard(),
			expected: true,
		},
		{
			board:    Board{0, 0, 1, 0, 0, 0, 0, 0, 0},
			expected: false,
		},
	}

	for _, test := range cases {
		if test.board.isEmpty() != test.expected {
			t.Errorf("Test %v failed: expected %t, but got %t", test, test.expected, !test.expected)
		}
	}
}

func TestCheckFreePlace(t *testing.T) {
	cases := []struct {
		place    Cell
		expected bool
		board    Board
	}{
		{
			2,
			true,
			NewBoard(),
		},
		{
			2,
			false,
			Board{0, 0, 1, 0, 0, 0, 0, 0, 0},
		},
	}

	for _, test := range cases {
		if test.board.isFreePlace(test.place) != test.expected {
			t.Errorf("Test %v failed: expected %t, but got %t", test, test.expected, !test.expected)
		}
	}
}

func TestValidMove(t *testing.T) {
	cases := []struct {
		place    Cell
		expected bool
		board    Board
	}{
		{
			2,
			true,
			NewBoard(),
		},
		{
			2,
			false,
			Board{0, 0, 1, 0, 0, 0, 0, 0, 0},
		},
		{
			-3,
			false,
			NewBoard(),
		},
		{
			11,
			false,
			NewBoard(),
		},
	}
	for _, test := range cases {
		if test.board.isValidMove(test.place) != test.expected {
			t.Errorf("Test %v failed: expected %t, but got %t", test, test.expected, !test.expected)
		}
	}
}

func TestMove(t *testing.T) {
	cases := []struct {
		place          Cell
		expected       error
		board          Board
		boardAfterTurn Board
	}{
		{
			2,
			nil,
			NewBoard(),
			Board{0, 0, 2, 0, 0, 0, 0, 0, 0},
		},
		{
			2,
			ErrInvalidMove,
			Board{0, 0, 1, 0, 0, 0, 0, 0, 0},
			Board{0, 0, 1, 0, 0, 0, 0, 0, 0},
		},
		{
			-3,
			ErrInvalidMove,
			NewBoard(),
			NewBoard(),
		},
		{
			11,
			ErrInvalidMove,
			NewBoard(),
			NewBoard(),
		},
	}
	for _, test := range cases {
		if err := test.board.Move(test.place, PlayerTwo); !errors.Is(err, test.expected) {
			t.Errorf("Test %v failed (wrong error): expected %s, but got %s", test, test.expected.Error(), err.Error())
		}

		if !reflect.DeepEqual(test.board, test.boardAfterTurn) {
			t.Errorf("Test %v failed (wrond board): expected %v, but got %v", test, test.boardAfterTurn, test.board)
		}
	}
}

func TestDoMultipleMove(t *testing.T) {
	board := NewBoard()

	board.Move(Cell(0), PlayerOne)
	board.Move(Cell(1), PlayerTwo)
	board.Move(Cell(3), PlayerOne)
	board.Move(Cell(7), PlayerTwo)

	expectedBoard := Board{1, 2, 0, 1, 0, 0, 0, 2, 0}

	if !equalBoards(expectedBoard, board) {
		t.Errorf(
			"Boards aren't equal: expected %v, but got %v",
			expectedBoard,
			board,
		)
	}
}

func TestBoardFull(t *testing.T) {
	cases := []struct {
		board    Board
		expected bool
	}{
		{
			board:    NewBoard(),
			expected: false,
		},
		{
			board: Board{
				1, 2, 1,
				1, 1, 2,
				2, 1, 2,
			},
			expected: true,
		},
	}

	for _, test := range cases {
		if full := test.board.isFull(); full != test.expected {
			t.Errorf(
				"Test %v failed: expected %t but got %t",
				test,
				test.expected,
				full,
			)
		}
	}
}

func TestWinner(t *testing.T) {
	cases := []struct {
		name     string
		board    Board
		expected Player
	}{
		{
			name:     "Empty board",
			board:    NewBoard(),
			expected: PlayerNil,
		},
		{
			name:     "Only first line",
			board:    Board{1, 1, 1, 0, 0, 0, 0, 0, 0},
			expected: PlayerOne,
		},
		{
			name:     "Only second line",
			board:    Board{0, 0, 0, 1, 1, 1, 0, 0, 0},
			expected: PlayerOne,
		},
		{
			name:     "Only third line",
			board:    Board{0, 0, 0, 0, 0, 0, 1, 1, 1},
			expected: PlayerOne,
		},
		{
			name:     "Only first column",
			board:    Board{1, 0, 0, 1, 0, 0, 1, 0, 0},
			expected: PlayerOne,
		},
		{
			name:     "Only second column",
			board:    Board{0, 1, 0, 0, 1, 0, 0, 1, 0},
			expected: PlayerOne,
		},
		{
			name:     "Only third column",
			board:    Board{0, 0, 1, 0, 0, 1, 0, 0, 1},
			expected: PlayerOne,
		},
		{
			name:     "Left diagonal",
			board:    Board{1, 0, 0, 0, 1, 0, 0, 0, 1},
			expected: PlayerOne,
		},
		{
			name:     "Right diagonal",
			board:    Board{0, 0, 1, 0, 1, 0, 1, 0, 0},
			expected: PlayerOne,
		},
		{
			name:     "Draw",
			board:    Board{1, 1, 2, 2, 2, 1, 1, 2, 2},
			expected: PlayerNil,
		},
		{
			name:     "Real game",
			board:    Board{2, 1, 0, 1, 2, 2, 1, 1, 1},
			expected: PlayerOne,
		},
		{
			name:     "Real game (Player two finaly won)",
			board:    Board{1, 0, 1, 2, 2, 2, 2, 1, 1},
			expected: PlayerTwo,
		},
	}

	for _, test := range cases {
		if winnerGot := test.board.getWinner(); winnerGot != test.expected {
			t.Errorf(
				"Test \"%s\" failed: expected %d, but got %d",
				test.name,
				test.expected,
				winnerGot,
			)
		}
	}
}

func TestGetPlayerOnCell(t *testing.T) {
	cases := []struct {
		board    Board
		expected Player
	}{
		{
			board:    NewBoard(),
			expected: PlayerNil,
		},
		{
			board:    Board{0, 0, 0, 1, 0, 0, 0, 0, 0},
			expected: PlayerOne,
		},
		{
			board:    Board{0, 0, 0, 2, 0, 0, 0, 0, 0},
			expected: PlayerTwo,
		},
	}

	for _, test := range cases {
		if player := test.board.getPlayer(Cell(3)); player != test.expected {
			t.Errorf(
				"Test %v failed: expected %d, but got %d",
				test,
				test.expected,
				player,
			)
		}
	}
}

func TestGameOver(t *testing.T) {
	cases := []struct {
		name             string
		board            Board
		expectedGameOver bool
		expectedWinner   Player
	}{
		{

			name:             "Beggin game",
			board:            NewBoard(),
			expectedGameOver: false,
			expectedWinner:   PlayerNil,
		},
		{
			name:             "Player One won",
			board:            Board{2, 1, 0, 1, 2, 2, 1, 1, 1},
			expectedGameOver: true,
			expectedWinner:   PlayerOne,
		},
		{
			name:             "Player Two won",
			board:            Board{1, 0, 1, 2, 2, 2, 2, 1, 1},
			expectedGameOver: true,
			expectedWinner:   PlayerTwo,
		},
		{
			name:             "The game isn't finish",
			board:            Board{1, 0, 0, 2, 1, 2, 1, 2, 0},
			expectedGameOver: false,
			expectedWinner:   PlayerNil,
		},
		{
			name:             "Draw",
			board:            Board{1, 1, 2, 2, 2, 1, 1, 2, 2},
			expectedGameOver: true,
			expectedWinner:   PlayerNil,
		},
	}

	for _, test := range cases {
		gameOver, winner := test.board.IsGameOver()
		if gameOver != test.expectedGameOver || winner != test.expectedWinner {
			t.Errorf(
				"Test %s failed: expected (%t, %d), but got (%t, %d)",
				test.name,
				test.expectedGameOver, test.expectedWinner,
				gameOver, winner,
			)
		}
	}
}

func equalBoards(b0, b1 Board) bool {
	return reflect.DeepEqual(b0, b1)
}
