package tictactoe

import (
	"errors"
	"reflect"
)

// Player represents one of the two player in the game
type Player int8

// Cell represents a cell of the tictactoe board
type Cell int8

const (
	PlayerNil Player = 0
	PlayerOne Player = 1
	PlayerTwo Player = 2
)

// Board represents a board of the tictactoe game
// 0, 1, 2,
// 3, 4, 5,
// 6, 7, 8
type Board []Cell

// NewBoard creates a nex tictactoe game
func NewBoard() Board {
	return make(Board, 9)
}

// IsEmpty checks if the board game is empty
func (b Board) isEmpty() bool {
	return reflect.DeepEqual(
		Board{0, 0, 0, 0, 0, 0, 0, 0, 0},
		b,
	)
}

// ErrInvalidMove is the error returned when a move is invalid
var ErrInvalidMove = errors.New("invalid move")

// Move places a player move on the board
func (b Board) Move(c Cell, player Player) error {
	if !b.isValidMove(c) {
		return ErrInvalidMove
	}
	b[c] = Cell(player)
	return nil
}

// IsValidMode returns true if a move is valid, false otherwise
func (b *Board) isValidMove(c Cell) bool {
	return 0 <= c && c <= 8 && b.isFreePlace(c)
}

// IsFreePlace returns true if the place isn't occupier, false otherwise
func (b Board) isFreePlace(c Cell) bool {
	return b[c] == 0
}

// IsFull returns true if there is no place left on the board, false otherwise
func (b Board) isFull() bool {
	for _, c := range b {
		if c == 0 {
			return false
		}
	}
	return true
}

// GetWinner returns the winner on the board
// or PlayerNil if there is no winner
func (b Board) getWinner() Player {
	lines := []struct {
		a Cell
		b Cell
		c Cell
	}{
		{0, 1, 2}, // First row
		{3, 4, 5}, // Second row
		{6, 7, 8}, // Third row

		{0, 3, 6}, // First column
		{1, 4, 7}, // Second column
		{2, 5, 8}, // Third column

		{0, 4, 8}, // Left diagonal
		{2, 4, 6}, // Right diagonal
	}

	for _, l := range lines {
		first, second, third := b.getPlayer(l.a), b.getPlayer(l.b), b.getPlayer(l.c)
		if first != PlayerNil && first == second && first == third {
			return first
		}
	}

	return PlayerNil
}

// GetPlayer returns the player on the cell
func (b Board) getPlayer(c Cell) Player {
	switch b[c] {
	case 1:
		return PlayerOne
	case 2:
		return PlayerTwo
	}
	return PlayerNil
}

// IsGameOver returns if the game is over and who is the player
func (b Board) IsGameOver() (bool, Player) {
	winner := b.getWinner()
	if winner != PlayerNil {
		return true, winner
	}

	if b.isFull() {
		return true, PlayerNil
	}

	return false, PlayerNil
}
