package main

import (
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"

	"gitlab.com/albandewilde/tictactoe/grpc/tictactoepb"
)

const HOST = "0.0.0.0"
const PORT = 4444

func main() {
	// Initialise listening connection
	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", HOST, PORT))
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	fmt.Println("Server start on " + lis.Addr().String())

	// Create and start the gRPC server
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	tictactoepb.RegisterTictactoeServer(grpcServer, newTictactoeServer())
	if err = grpcServer.Serve(lis); err != nil {
		log.Fatal(err)
	}
}
